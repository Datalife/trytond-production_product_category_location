====
Check default and allowed production location for categories
====

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install team_timesheet Module::

    >>> config = activate_modules('production_product_category_location')


Check default for warehouse field while allow new category to a location::

    >>> Location = Model.get('stock.location')
    >>> warehouse = Location.find([('code', '=', 'WH')])[0]
    >>> production = Location.find([('code','=','PROD')])[0]
    >>> warehouse.production_location = production
    >>> warehouse.save()
    >>> test_production = Location(code='PROD_TEST', name='Production location test', type='production', parent=production)
    >>> test_production.save()
    >>> test_production.reload()
    >>> cat_loc = test_production.categories.new()
    >>> cat_loc.warehouse == warehouse
    True
    >>> Category = Model.get('product.category')
    >>> cat = Category(name='test cat')
    >>> cat.save()
    >>> cat_loc.category = cat
    >>> test_production.save()

Check sequence compute in locations form::

    >>> test_production.reload()
    >>> test_production.categories[0].sequence
    1
    >>> cat_loc = test_production.categories.new()
    >>> cat = Category(name='test cat2')
    >>> cat.save()
    >>> cat_loc.category = cat
    >>> test_production.save()
    >>> test_production.reload()
    >>> test_production.categories[0].sequence
    1
    >>> test_production.categories[1].sequence
    1
    >>> test_production = Location(code='PROD_TEST2', name='Production location test 2', type='production', parent=production)
    >>> test_production.save()
    >>> test_production.reload()
    >>> cat_loc = test_production.categories.new()
    >>> cat_loc.category = cat
    >>> test_production.save()
    >>> test_production.reload()
    >>> test_production.categories[0].sequence
    2