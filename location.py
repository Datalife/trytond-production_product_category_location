# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pyson import And, Eval, Equal, Not
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['ProductCategoryLocation', 'Location']


class ProductCategoryLocation(metaclass=PoolMeta):
    __name__ = 'stock.product.category.location'

    @classmethod
    def __setup__(cls):
        super(ProductCategoryLocation, cls).__setup__()
        location_dom = cls.location.domain
        cls.location.domain = [
                                  'OR',
                                  location_dom,
                                  [
                                      ('type', '=', 'production'),
                                      ('parent', '=', Eval('_parent_warehouse', {}).get('production_location', None))
                                  ]
        ]

    @classmethod
    def default_warehouse(cls):
        loc_id = Transaction().context.get('current_location', None)
        if loc_id:
            location_pool = Pool().get('stock.location')
            loc = location_pool(loc_id)
            if loc.type == 'production':
                warehouse = location_pool.search([
                    ('type', '=', 'warehouse'),
                    ('production_location', '=', loc.parent.id)
                ])
                if warehouse:
                    return warehouse[0].id
            else:
                return super(ProductCategoryLocation, cls).default_warehouse()
        return None


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    @classmethod
    def __setup__(cls):
        super(Location, cls).__setup__()
        inv_state = cls.categories.states["invisible"]
        cls.categories.states["invisible"] = And(
            inv_state, Not(Equal(Eval('type'), 'production')))

    @classmethod
    def view_attributes(cls):
        res = super(Location, cls).view_attributes()
        for item in res:
            if item[0] == '//page[@id="categories"]':
                item[2]['invisible'] = And(
                    item[2]['invisible'], Eval('type') != 'production')
                break
        return res
