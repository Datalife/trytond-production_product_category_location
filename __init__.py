# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .location import Location, ProductCategoryLocation

def register():
    Pool.register(
        Location,
        ProductCategoryLocation,
        module='production_product_category_location', type_='model')
